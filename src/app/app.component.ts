import { Component } from '@angular/core';
import { TransportationService } from './transportation.service';
import { Car } from './car';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  prefix = "I'm a ";
  name = 'star student';

  cars: Car[];

  counter: number = 0;
  phrase = "It's going";
  username: string = '';

  make: string = '';
  model: string = '';
  miles: number = 0;

  constructor(private transportationService: TransportationService) {
    this.cars = this.transportationService.getCars();
  }

  speak() {
    const sentence = this.prefix + this.name;
    return sentence;
  }

  increment() {
    this.counter++;
  }

  updateText() {
    this.phrase += ' ...and going';
  }

  addCar() {
    const newCar: Car = {
      make: this.make,
      model: this.model,
      miles: this.miles,
    };

    this.transportationService.addCar(newCar);

    this.make = '';
    this.model = '';
    this.miles = 0;
  }
}
